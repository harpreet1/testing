import {suite, test} from "mocha-typescript";
import {MovieService} from "../../src/services/movie.service";
import {MovieRepository} from "../../src/repository/movie.repository";
import {instance, mock, verify, when} from "ts-mockito";
import {Movie, MovieStatus} from "../../src/modules/movie";
import {assert, expect} from "chai";

@suite
export class MovieServiceSpec{
    @test
    public shouldGetAvailableMovies(): void {
        const movieRepositoryMock = mock(MovieRepository);
        const movieServiceInstance = new MovieService(instance(movieRepositoryMock));
        const movie1 = new  Movie(1, "abc",2018,'director',1, MovieStatus.Available);
        const movie2 = new  Movie(2, "def",2018,'director',1, MovieStatus.CheckedOut);
        const movie3 = new  Movie(3, "xyz",2018,'director',1, MovieStatus.CheckedOut);
        when(movieRepositoryMock.getMoviesByStatus(MovieStatus.Available)).thenReturn([movie1]);

        const availableMovies = movieServiceInstance.listAvailableMovies(MovieStatus.Available);

        expect(availableMovies).to.deep.include(movie1);
        expect(availableMovies).to.not.include(movie2);
        expect(availableMovies).to.not.include(movie3);
    }

    @test
    public shouldCheckOutAMovie(): void {
        const movieRepositoryMock = mock(MovieRepository);
        const movieServiceInstance = new MovieService(instance(movieRepositoryMock));
        const oldStatusMovie = new  Movie(1, "abc",2018,'director',1, MovieStatus.Available);
        const newStatusMovie = new  Movie(1, "def",2018,'director',1, MovieStatus.CheckedOut);
        when(movieRepositoryMock.getTheMovie(oldStatusMovie.id)).thenReturn(oldStatusMovie);
        when(movieRepositoryMock.updateTheMovie(newStatusMovie.id)).thenReturn(newStatusMovie);

        movieServiceInstance.checkOutTheMovie(oldStatusMovie.id);

        verify(movieRepositoryMock.updateTheMovie(newStatusMovie.id)).once();
    }
    @test
    public shouldThrowErrorIfTheMovieIsNotAvailable(): void {
        const movieRepositoryMock = mock(MovieRepository);
        const movieServiceInstance = new MovieService(instance(movieRepositoryMock));

        assert.throws(() => movieServiceInstance.checkOutTheMovie(undefined),Error,"Movie not available");
        assert.throws(() => movieServiceInstance.checkOutTheMovie(null),Error,"Movie not available");
    }
}