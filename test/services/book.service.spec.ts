import {BookService} from "../../src/services/book.service";
import {assert, expect} from 'chai';
import {BookRepository} from "../../src/repository/book.repository";
import {suite, test} from "mocha-typescript";
import {deepEqual, instance, mock, spy, verify, when} from "ts-mockito";
import {Book, BookStatus} from "../../src/modules/book";
import {BookDoesNotExistError} from "../../src/Error/bookDoesNotExistError";
import {BookIsNotAvailableError} from "../../src/Error/bookIsNotAvailableError";

@suite
export class BookServiceSpec {
    private bookRepository: BookRepository;
    private bookService: BookServiceSpecImpl;

    public before(): void {
        this.bookRepository = mock(BookRepository);
        this.bookService = new BookServiceSpecImpl(instance(this.bookRepository));
    }

    @test
    public shouldGetAListOfBooks(): void {
        const books = [new Book(1, "Five point someone"), new Book(2, "The Alchemist")];
        when(this.bookRepository.getAllBooks()).thenReturn(books);

        const actualBooks = this.bookService.getAllBooks();

        expect(actualBooks).to.deep.equals(books);
    }

    @test
    public shouldCheckOutABook(): void {
        const spiedService = spy(this.bookService);
        const oldStatusBook = new Book(1, "Five point someone", BookStatus.Available);
        const newStatusBook = new Book(1, "Five point someone", BookStatus.CheckedOut);
        const libraryId = "lib-1234";
        when(spiedService.validateLibraryId(libraryId)).thenReturn(true);
        when(this.bookRepository.getABook(oldStatusBook.id)).thenReturn(oldStatusBook);
        when(this.bookRepository.updateBook(deepEqual(newStatusBook))).thenReturn(newStatusBook);

        this.bookService.checkOutABook(oldStatusBook.id,libraryId);

        verify(spiedService.validateLibraryId(libraryId)).once();
        verify(this.bookRepository.updateBook(deepEqual(newStatusBook))).once();
    }

    @test
    public shouldReturnABookDoesNotExistError(): void {
        const oldStatusBook = new Book(1, "Five point someone", BookStatus.Available);
        const libraryId = "lib-1234";
        when(this.bookRepository.getABook(oldStatusBook.id)).thenReturn(null);

        assert.throws(() => this.bookService.checkOutABook(1,libraryId),BookDoesNotExistError,"Book does not exist");
    }
    @test
    public shouldReturnABookNotAvailableError(): void {
        const oldStatusBook = new Book(1, "Five point someone", BookStatus.Available);
        const checkedOutBook = new Book(1, "Five point someone", BookStatus.CheckedOut);
        const libraryId = "lib-1234";
        when(this.bookRepository.getABook(oldStatusBook.id)).thenReturn(checkedOutBook);

        assert.throws(() => this.bookService.checkOutABook(1,libraryId),BookIsNotAvailableError,"Book is not available");
    }

    @test
    public shouldGetAvailableBooks(): void {
        this.bookService.getBooksByStatus(BookStatus.Available);

        verify(this.bookRepository.getBooksByStatus(BookStatus.Available)).once();
    }

    @test
    public shouldReturnABook(): void {
        const oldStatusBook = new Book(1, "Five point someone", BookStatus.CheckedOut);
        const newStatusBook = new Book(1, "Five point someone", BookStatus.Available);
        when(this.bookRepository.getABook(oldStatusBook.id)).thenReturn(oldStatusBook);
        when(this.bookRepository.updateBook(deepEqual(newStatusBook))).thenReturn(newStatusBook);

        this.bookService.returnABook(oldStatusBook.id, "lib-1234");

        verify(this.bookRepository.updateBook(deepEqual(newStatusBook))).once();
    }
}
class BookServiceSpecImpl extends BookService{

    public validateLibraryId(libraryId: string): Boolean {
        return super.validateLibraryId(libraryId);
    }
}