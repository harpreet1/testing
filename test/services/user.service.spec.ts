import {suite, test} from "mocha-typescript";
import {assert, expect} from "chai";
import {UserService} from "../../src/services/user.service";
import {UserRepository} from "../../src/repository/user.repository";
import {instance, mock} from "ts-mockito";

@suite
export class UserServiceSpec {
    @test
    public shouldGetTheAppropriateUserFromRepository(): void {
        const userService = new UserService(instance(mock(UserRepository)));
        var libraryId = "lib-1234";
        var password = "password";

        const actualUser = userService.getTheUser(libraryId, password);

        expect(actualUser.getLibraryId()).to.equal(libraryId);
        expect(actualUser.getPassword()).to.equal(password);
    }

    @test
    public shouldThrowErrorWhenLibraryIdAndPasswordMismatch(): void {
        const userService = new UserService(instance(mock(UserRepository)));
        var libraryId = "lib-1234";
        var password = "password1";

        const actualUser = userService.getTheUser(libraryId, password);

        expect(actualUser).to.be.equal(undefined);
    }
}