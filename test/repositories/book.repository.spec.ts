import {suite, test} from "mocha-typescript";
import {BookRepository} from "../../src/repository/book.repository";
import {expect} from "chai";
import {Book, BookStatus} from "../../src/modules/book";

@suite
export class BookRepositorySpec {
    @test
    public shouldGetAllBooks(): void {
        const newRepository = new BookRepository();

        const allBooks = newRepository.getAllBooks();

        expect(allBooks).to.be.of.length(2);
    }

    @test
    public shouldGetOneBook(): void {
        const newRepository = new BookRepository();
        const expectedFirstBook = new Book(1,"Five Point Someone");

        const oneBook = newRepository.getABook(expectedFirstBook.id);

        expect(oneBook).to.deep.equals(expectedFirstBook);
    }


    @test
    public shouldGetBooksBasedOnStatus(): void {
        const newRepository = new BookRepository();
        const bookOne = new Book(1,'Five Point Someone',BookStatus.Available);
        const bookTwo = new Book(2,'The Secret',BookStatus.CheckedOut);

        const allBooks = newRepository.getBooksByStatus(BookStatus.Available);

        expect(allBooks).to.be.of.length(1);
        expect(allBooks).to.deep.include(bookOne);
        expect(allBooks).not.to.include(bookTwo);

    }

    @test
    public shouldUpdateOneBook(): void {
        //given
        const newRepository = new BookRepository();
        const bookToBeUpdated = new Book(1,"Five Point Someone",BookStatus.CheckedOut);

        //when
        const updatedBook = newRepository.updateBook(bookToBeUpdated);

        //then
        const allBooks = newRepository.getAllBooks();
        expect(updatedBook).to.equals(bookToBeUpdated);
        expect(allBooks).to.include(updatedBook);
        expect(allBooks).to.length(2);
    }


}