import {suite, test} from "mocha-typescript";
import {expect} from "chai";
import {UserRepository} from "../../src/repository/user.repository";

@suite

export class UserRepositorySpec {
    private userRepository: UserRepository;
    @test
    public shouldGetTheUserWhenPresent(): void {
        this.userRepository = new UserRepository();
        var libraryId = "lib-1234";
        var password = "password";

        const actualUserId = this.userRepository.getTheUser(libraryId, password);

        expect(actualUserId.getLibraryId()).to.equal(libraryId);
        expect(actualUserId.getPassword()).to.equal(password);

    }
}