import {suite, test} from "mocha-typescript";
import {expect} from "chai";
import {MovieRepository} from "../../src/repository/movie.repository";
import {Movie, MovieStatus} from "../../src/modules/movie";

@suite
export class MovieRepositorySpec{
    @test
    public shouldGetMoviesWithStatusAvailable(): void {
        const newRepository = new MovieRepository();
        const movieOne = new Movie(1,'abc',2018,'director',1,MovieStatus.Available);
        const movieTwo = new Movie(2,'def',2018,'director',1,MovieStatus.CheckedOut);
        const movieThree = new Movie(3,'xyz',2018,'director',1,MovieStatus.CheckedOut);

        const allAvailableMovies = newRepository.getMoviesByStatus(MovieStatus.Available);

        expect(allAvailableMovies).length(1);
        expect(allAvailableMovies).to.deep.include(movieOne);
        expect(allAvailableMovies).not.to.include(movieTwo);
        expect(allAvailableMovies).not.to.include(movieThree);

    }

    @test
    public shouldGetOneMovie(): void {
        const newRepository = new MovieRepository();
        const movie = new Movie(1,'abc',2018,'director',1,MovieStatus.Available);

        const oneMovie = newRepository.getTheMovie(movie.id);

        expect(oneMovie).to.deep.equals(movie);
    }

    @test
    public shouldUpdateTheStatusOfMovieToCheckedOut(): void {
        //given
        const newRepository = new MovieRepository();
        const availableMovie = new Movie(1,'abc',2018,'director',1,MovieStatus.Available);
        const checkedOutMovie = new Movie(1,'abc',2018,'director',1,MovieStatus.CheckedOut);
        //when
        const updatedMovie = newRepository.updateTheMovie(availableMovie.id);
        //then
        //const allAvailableMovies = newRepository.getMoviesByStatus(MovieStatus.Available);
        expect(updatedMovie).to.deep.equals(checkedOutMovie);
        //expect(allAvailableMovies).to.deep.include(availableMovie);
        //expect(allAvailableMovies).to.length(1);
    }
}
