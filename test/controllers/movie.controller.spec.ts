import {suite, test} from "mocha-typescript";
import {MovieService} from "../../src/services/movie.service";
import {instance, mock, verify, when} from "ts-mockito";
import {MovieController} from "../../src/controllers/movie.controller";
import {Movie, MovieStatus} from "../../src/modules/movie";
import {assert, expect} from "chai";

@suite
export class MovieControllerSpec {
    private mockMovieService: MovieService;
    private movieControllerInstance: MovieController;

    @test
    public shouldGetAListOfAvailableMovies(): void {
        this.mockMovieService = mock(MovieService);
        this.movieControllerInstance = new MovieController(instance(this.mockMovieService));
        const movie1 = new Movie(1, "abc", 2018, 'director', 1, MovieStatus.Available);
        const movie2 = new Movie(2, "def", 2018, 'director', 1, MovieStatus.CheckedOut);
        const movie3 = new Movie(3, "xyz", 2018, 'director', 1, MovieStatus.CheckedOut);
        when(this.mockMovieService.listAvailableMovies(MovieStatus.Available)).thenReturn([movie1]);

        const availableMovies = this.movieControllerInstance.listAvailableMovies(MovieStatus.Available);

        expect(availableMovies).to.deep.include(movie1);
        expect(availableMovies).to.not.include(movie2);
        expect(availableMovies).to.not.include(movie3);
    }

    @test
    public shouldThrowAnErrorWhenTheStatusIsInvalid(): void {
        this.mockMovieService = mock(MovieService);
        this.movieControllerInstance = new MovieController(instance(this.mockMovieService));
        const invalidStatus = 'randomString';

        assert.throws(() => this.movieControllerInstance.listAvailableMovies(invalidStatus), Error, 'Bad request.');

    }

    @test
    public shouldCheckOutGivenBook(): void {
        this.mockMovieService = mock(MovieService);
        this.movieControllerInstance = new MovieController(instance(this.mockMovieService));

        this.movieControllerInstance.checkOutTheMovie(1);

        verify(this.mockMovieService.checkOutTheMovie(1)).once();
    }


    @test
    public shouldThrowErrorIfIdIsInvalid(): void {
        this.mockMovieService = mock(MovieService);
        this.movieControllerInstance = new MovieController(instance(this.mockMovieService));

        assert.throws(() => this.movieControllerInstance.checkOutTheMovie(-1), Error, "Received invalid movie id");
        assert.throws(() => this.movieControllerInstance.checkOutTheMovie(undefined), Error, "Received invalid movie id");
        assert.throws(() => this.movieControllerInstance.checkOutTheMovie(null), Error, "Received invalid movie id");


    }
}