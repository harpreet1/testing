import {suite, test} from "mocha-typescript";
import {BookController} from "../../src/controllers/book.controller";
import {assert, expect} from "chai";
import {instance, mock, verify, when} from "ts-mockito";
import {BookService} from "../../src/services/book.service";
import {Book, BookStatus} from "../../src/modules/book";
import {BookIdNotAvailableError} from '../../src/Error/bookIdNotAvailableError';

@suite
export class BookControllerSpec {

    @test
    public shouldGetAllBooks(): void {
        //given
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));
        const books = [new Book(1,"Five point someone"), new Book(2,"The Alchemist")];
        when(bookService.getAllBooks()).thenReturn(books);

        //when
        const actualBooks = bookController.getAllBooks();

        //then
        expect(actualBooks).to.equals(books);
    }

    @test
    public  shouldCheckOutGivenBook(): void {
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));

        bookController.checkOutABook(1,"lib-1234");

        verify(bookService.checkOutABook(1, "lib-1234")).once();

    }

    @test
    public shouldThrowErrorIfIdIsNullOrUndefined(): void {
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));

        assert.throws(() => bookController.checkOutABook(undefined, "lib-1234"),BookIdNotAvailableError,"Book id not available");

    }

    @test
    public shouldThrowErrorIfIdIsNegative(): void {
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));

        assert.throws(() => bookController.checkOutABook(-1, "lib-1234"),BookIdNotAvailableError,"Book id not available");

    }

    @test
    public shouldGetAvailableBooks(): void {
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));
        const status = BookStatus.Available;

        bookController.getBooksByStatus(status);

        verify(bookService.getBooksByStatus(status)).once();
    }
    @test
    public shouldReturnABook(): void {
        const bookService = mock(BookService);
        const bookController = new BookController(instance(bookService));

        bookController.returnABook(1, "lib-1234");

        verify(bookService.returnABook(1, "lib-1234")).once();

    }
}