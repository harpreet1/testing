import {suite, test} from "mocha-typescript";
import {expect} from "chai";
import {IdGenerator} from "../../src/util/id-generator";

@suite
export class IdGeneratorSpec {
    @test
    public shouldGenerateNewId(): void {
        const idGenerator = new IdGenerator();
        const beforeTime = Date.now();

        const actualID = idGenerator.generateId();

        expect(actualID).to.gte(beforeTime);
        expect(actualID).to.lte(Date.now());
    }
}