import express = require('express');
import {BookController} from "./controllers/book.controller";
import {BookService} from "./services/book.service";
import {BookRepository} from "./repository/book.repository";
import {MovieController} from "./controllers/movie.controller";
import {MovieService} from "./services/movie.service";
import {MovieRepository} from "./repository/movie.repository";

const app = express();

const loggedIn = false;

app.get('/books', (req, res) => {
    try {
        if (!req.query.status) {
            const allBooks = new BookController(new BookService(new BookRepository())).getAllBooks();
            res.send(allBooks);
        }
        else if (req.query.status === 'available') {
            const allBooks = new BookController(new BookService(new BookRepository())).getBooksByStatus('available');
            res.send(allBooks);
        } else {
            throw new Error('Invalid Input');
        }
    } catch (error) {
        return res.status(400).send(error.message);
    }

});


app.patch('/book/:bookId/:libraryId/checkout', (req, res) => {
    const bookControllerInstance = new BookController(new BookService(new BookRepository()));
    const bookId = parseInt(req.params.bookId);
    const libraryId = JSON.stringify(req.params.libraryId);
    try {
        bookControllerInstance.checkOutABook(bookId, libraryId);
        return res.status(200).send('Book Checked Out');
    } catch (error) {
        return res.status(400).send(error.message);
        return res.status(404).send(error.message);
        return res.status(403).send(error.message);
    }
});

app.patch('/book/:bookId/:libraryId/return', (req, res) => {
    const bookControllerInstance = new BookController(new BookService(new BookRepository()));
    const bookId = parseInt(req.params.bookId);
    const libraryId = JSON.stringify(req.params.libraryId);
    try {
        bookControllerInstance.returnABook(bookId, libraryId);
        return res.status(200).send('Book Returned');
    } catch (error) {
        return res.status(400).send(error.message);
        return res.status(404).send(error.message);
        return res.status(403).send(error.message);
    }
});

app.get('/movies', (req, res) => {
try {
        const availableMovies = new MovieController(new MovieService(new MovieRepository())).listAvailableMovies('available');
        res.send(availableMovies);
    }catch (error){
    return res.status(400).send(error.message);
}
});

app.patch('/movies/:movieId/:libraryId/checkout', (req, res) => {
    const movieControllerInstance = new MovieController(new MovieService(new MovieRepository()));
    const movieId = parseInt(req.params.movieId);
    try {
        movieControllerInstance.checkOutTheMovie(movieId);
        return res.status(200).send('Movie Checked Out');
    } catch (error) {
        return res.status(400).send(error.message);
    }
});

app.listen(3000, () => console.log('Listening on port 3000!'));
