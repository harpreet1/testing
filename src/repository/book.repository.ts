import {Book, BookStatus} from "../modules/book";

export class BookRepository {


    private static bookDB: Book[] = [new Book(1, "Five Point Someone"), new Book(2, "The Secret", BookStatus.CheckedOut)];

    public getAllBooks(): Book[] {
        return BookRepository.bookDB;
    }

    public updateBook(book: Book): Book {
        const id = book.id;
        const bookIndex = BookRepository.bookDB.findIndex((book) => (book.id === id));
        BookRepository.bookDB[bookIndex] = book;
        return BookRepository.bookDB[bookIndex];
    }

    public getABook(bookId: number): Book {
        const bookIndex = BookRepository.bookDB.findIndex((book) => (book.id === bookId));
        return BookRepository.bookDB[bookIndex];
    }

    public getBooksByStatus(status: BookStatus): Book[] {
        return BookRepository.bookDB.filter(
            (book) => {return book.getBookStatus() === status});
    }
}


//MongoDB uses a plugin that creates a created at/updated at field every Time an event on the DB occurs.
//So, in our case, we will keep updating only a "created at" instead of creating a "updated at" field.