import {User} from "../modules/user";

export class UserRepository {


    private static userDB: User[] = [new User('lib-1234', 'password'), new User('lib-5678', 'password1' )];

    public getTheUser (libraryId, password): User {
        const user = UserRepository.userDB.filter((user) => {if(user.getLibraryId() === libraryId && user.getPassword() === password){
            return user;
        }});
        return user[0];
    }
}


//MongoDB uses a plugin that creates a created at/updated at field every Time an event on the DB occurs.
//So, in our case, we will keep updating only a "created at" instead of creating a "updated at" field.