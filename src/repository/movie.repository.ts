import {Movie, MovieStatus} from "../modules/movie";


export class MovieRepository{

    private static  movieDB: Movie[] = [new  Movie(1, "abc",2018,'director',1, MovieStatus.Available),
        new  Movie(2, "def",2018,'director',1, MovieStatus.CheckedOut),
        new  Movie(3, "xyz",2018,'director',1, MovieStatus.CheckedOut)]

    public getMoviesByStatus(status: MovieStatus): Movie[] {
        return MovieRepository.movieDB.filter(
            (movie) =>  movie.getMovieStatus() === status);
    }

    public getTheMovie(id: number): Movie {
        const movieId =  MovieRepository.movieDB.findIndex((movie) => (movie.id === id));
        return MovieRepository.movieDB[movieId];
    }

    public updateTheMovie(id: number): Movie {
        const movie = this.getTheMovie(id);
        movie.checkOutTheMovie();
        return movie;
    }
}