import {Book} from "../modules/book";
import {BookService} from "../services/book.service";
import {BookIdNotAvailableError} from "../Error/bookIdNotAvailableError";


export class BookController {
    private bookServiceInstance: BookService;

    constructor(bookServiceInstance: BookService) {
        this.bookServiceInstance = bookServiceInstance;
    }

    public getAllBooks(): Book[] {
        return this.bookServiceInstance.getAllBooks();
    }

    public checkOutABook(bookId: number, libraryId: string): void {
        if (bookId && bookId > 0) {
            this.bookServiceInstance.checkOutABook(bookId,libraryId);
        } else {
            throw new BookIdNotAvailableError('Book id not available');
        }
    }

    public getBooksByStatus(status: string): Book[] {
        if (status) {
            return this.bookServiceInstance.getBooksByStatus(status);
        }
        throw new Error ('Invalid Input');

    }

    public returnABook(bookId: number, libraryId: string) {
        if (bookId && bookId > 0) {
            this.bookServiceInstance.returnABook(bookId, libraryId);
        } else {
            throw new BookIdNotAvailableError('Book id not available');
        }
    }
}