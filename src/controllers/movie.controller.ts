import {MovieService} from "../services/movie.service";
import {Movie, MovieStatus} from "../modules/movie";

export class MovieController {

    private movieService: MovieService;

    constructor(movieService: MovieService) {
        this.movieService = movieService;
    }

    public listAvailableMovies(status: string): Movie[] {
        if (status === MovieStatus.Available) {
            return this.movieService.listAvailableMovies(MovieStatus.Available);
        } else {
            throw Error('Bad request.');
        }
    }

    public checkOutTheMovie(id: number): void {
        if(id && id>0) {
            this.movieService.checkOutTheMovie(id);
        }else{
            throw new Error("Received invalid movie id");
        }
    }
}