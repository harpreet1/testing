export class Book {
    public readonly id: number;
    private readonly name: string;
    private libraryId: string;
    private status: BookStatus;

    constructor(id: number, name: string, status: BookStatus = BookStatus.Available) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public isCheckedOut(): boolean{
        return this.status === BookStatus.CheckedOut ? true :false;
    }
    public checkout(){
        this.status = BookStatus.CheckedOut;
    }
    public available(){
        this.status = BookStatus.Available;
    }
    public getBookStatus(): BookStatus{
        return this.status;
    }


}

export enum BookStatus {
    Available = "available",
    CheckedOut = "checkedOut"
    //remove the string = and check once
}




