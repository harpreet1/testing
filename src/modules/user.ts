export class User{
    private libraryId: string;
    private password: string;

    constructor(libraryId: string, password: string) {
        this.libraryId = libraryId;
        this.password = password;
    }

    public getLibraryId() : string {
        return this.libraryId;
    }

    public getPassword(): string {
        return this.password;
    }
}