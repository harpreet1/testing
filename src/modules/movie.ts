export class Movie{
    public readonly id: number;
    private status: MovieStatus;
    private rating: number;
    private name: string;
    private year: number;
    private director: string;

    constructor(id: number, name: string, year: number, director: string, rating:  0 |1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10, status: MovieStatus = MovieStatus.Available) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
        this.status = status;
    }

    public checkOutTheMovie() {
        this.status = MovieStatus.CheckedOut;
    }

    public getMovieStatus() {
        return this.status;
    }
}

export enum MovieStatus{
    Available = "available",
    CheckedOut = "checkedout"
}
