import {UserRepository} from "../repository/user.repository";
import {User} from "../modules/user";

export class UserService {
    private userRepository: UserRepository;
    constructor(userRepository : UserRepository) {
        this.userRepository = new UserRepository();
    }

    public getTheUser(libraryId: string, password: string): User {
            return this.userRepository.getTheUser(libraryId, password);
}
}