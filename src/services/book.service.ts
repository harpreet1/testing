import {Book, BookStatus} from "../modules/book";
import {BookRepository} from "../repository/book.repository";
import {BookDoesNotExistError} from "../Error/bookDoesNotExistError";
import {BookIsNotAvailableError} from "../Error/bookIsNotAvailableError";

export class BookService {
    private bookRepository: BookRepository;

    constructor(bookRepository: BookRepository) {
        this.bookRepository = bookRepository;
    }

    public getAllBooks(): Book[] {
        return this.bookRepository.getAllBooks();
    }

    public checkOutABook(bookIdToBeCheckedOut: number, libraryId: string): void {
        this.validateLibraryId(libraryId);
        const bookToBeCheckedOut = this.getABook(bookIdToBeCheckedOut);
            if(!bookToBeCheckedOut.isCheckedOut()) {
                bookToBeCheckedOut.checkout();
                this.bookRepository.updateBook(bookToBeCheckedOut);
            }
            else{
                throw new BookIsNotAvailableError('Book is not available');
            }
        }

    public getBooksByStatus(status: string): Book[] {
        const bookStatus = this.getFilterStatus(status);
        return this.bookRepository.getBooksByStatus(bookStatus);
    }

    public returnABook(bookIdToBeReturned: number, libraryId: string) {
        this.validateLibraryId(libraryId);
        const bookToBeReturned = this.getABook(bookIdToBeReturned);
            if(bookToBeReturned.isCheckedOut()) {
                bookToBeReturned.available();
                this.bookRepository.updateBook(bookToBeReturned);
            }
            else{
                throw new BookIsNotAvailableError('Book is not available');
            }
    }


    protected validateLibraryId(libraryId: string): Boolean {
        return (libraryId.length == 8) && libraryId.startsWith('LIB-') && (!(isNaN(+libraryId.substring(4,7))));
    }

    private getABook(bookId: number): Book {
        const book = this.bookRepository.getABook(bookId);
        if(book){
            return book;
        }else {
            throw new BookDoesNotExistError('Book does not exist.');
        }
    }

    private getFilterStatus(status: string) {
        if(BookStatus.Available.toUpperCase() == status.toUpperCase()) return BookStatus.Available;
        else if(BookStatus.CheckedOut.toUpperCase() == status.toUpperCase()) return BookStatus.CheckedOut;
        else throw Error("Book Status not available.");

    }

}