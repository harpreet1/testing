import {MovieRepository} from "../repository/movie.repository";
import {Movie, MovieStatus} from "../modules/movie";

export class MovieService {
    private movieRepository: MovieRepository;

    constructor(movieRepository: MovieRepository) {
        this.movieRepository = movieRepository;
    }

    public listAvailableMovies(status: MovieStatus): Movie[] {
        return this.movieRepository.getMoviesByStatus(status);
    }

    public checkOutTheMovie(id: number) {
        const theMovie = this.movieRepository.getTheMovie(id);
        if(theMovie && theMovie.getMovieStatus() === MovieStatus.Available){
            theMovie.checkOutTheMovie();
            this.movieRepository.updateTheMovie(id);
        }else{
            throw new Error('Movie not available.')
        }

    }
}