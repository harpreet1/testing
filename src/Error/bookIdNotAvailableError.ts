export class BookIdNotAvailableError extends Error{
    constructor(message: string){
        super(message);
    }
}