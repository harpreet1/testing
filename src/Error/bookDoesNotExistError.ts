export class BookDoesNotExistError extends Error{
    constructor(message: string){
        super(message);
    }
}