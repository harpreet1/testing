export class BookIsNotAvailableError extends Error{
    constructor(message: string){
        super(message);
    }
}